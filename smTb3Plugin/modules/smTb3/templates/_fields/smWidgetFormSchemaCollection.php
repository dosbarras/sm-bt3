<?php $renderCollectionForm = function($form, $allowDelete = true) use($sf_params) { ?>
  <div class="panel panel-default" data-collection-item>
    <div class="panel-body" style="padding-bottom:0;">
      
      <?php if ($allowDelete): ?>
        <button type="button" class="btn btn-default btn-xs" style="position:absolute; z-index:9999;" data-collection-remove>
          <span class="glyphicon glyphicon-remove"></span>
        </button>
      <?php endif ?>
      
      <?php echo $form->renderHiddenFields(false) ?>
      
      <?php foreach ($form as $name => $field): ?>
        <?php if ($field->isHidden()) continue ?>
        <?php try { ?>
          <?php include_partial('smTb3/rows/'.get_class($field->getWidget()), array('field' => $field)) ?>
        <?php } catch(sfRenderException $e) { ?>
          <?php include_partial('smTb3/rows/default', array('field' => $field)) ?>
        <?php } ?>
      <?php endforeach ?>
      
    </div>
  </div>
<?php } ?>

<script type="text/html" data-collection-prototype="<?php echo $field->getName() ?>">
  <?php $renderCollectionForm($field['__prototype__'], $field->getWidget()->getAttribute('allow_delete')) ?>
</script>

<div data-collection-container="<?php echo $field->getName() ?>">
  <?php foreach ($field as $name => $form): ?>
    <?php if ($name == '__prototype__') continue ?>
    <?php $renderCollectionForm($form, $field->getWidget()->getAttribute('allow_delete')) ?>
  <?php endforeach ?>  
</div>

<?php if ($field->getWidget()->getAttribute('allow_add')): ?>
  <button type="button" class="btn btn-default btn-sm" data-collection-add="<?php echo $field->getName() ?>">
    <span class="glyphicon glyphicon-plus"></span>
  </button>
<?php endif ?>

