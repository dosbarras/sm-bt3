<?php 

$attributes = $attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes;
$attributes['class'] = isset($attributes['class']) ? $attributes['class'].' ' : '';
$attributes['class'] .= 'form-control';

echo $field->render($attributes);