<?php 

$attributes = $attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes;

$attributes['class'] = isset($attributes['class']) ? $attributes['class'].' ' : '';
$attributes['class'] .= 'form-control';

$attributes['style'] = isset($attributes['style']) ? $attributes['style'].' ' : '';
$attributes['style'] .= 'width:80px; display:inline-block;';

$field->getWidget()->setOption('template', 'from %from_date% to %to_date%');
$field->getWidget()->setOption('filter_template', '%date_range% %empty_checkbox% %empty_label%');

echo $field->render($attributes);