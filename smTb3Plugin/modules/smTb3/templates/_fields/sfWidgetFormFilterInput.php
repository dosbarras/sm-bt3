<?php 

$attributes = $attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes;
$attributes['class'] = isset($attributes['class']) ? $attributes['class'].' ' : '';
$attributes['class'] .= 'form-control';

$field->getWidget()->setOption('template', '<div class="input-group">%input% <span class="input-group-addon" id="basic-addon2">%empty_checkbox% %empty_label%</span></div>');

echo $field->render($attributes);