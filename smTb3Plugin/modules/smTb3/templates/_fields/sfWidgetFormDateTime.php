<?php 

$attributes = $attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes;

$attributes['date'] = array('class' => 'form-control', 'style' => 'width:80px; display:inline-block;');
$attributes['time'] = array('class' => 'form-control', 'style' => 'width:65px; display:inline-block;');  

echo $field->render($attributes);