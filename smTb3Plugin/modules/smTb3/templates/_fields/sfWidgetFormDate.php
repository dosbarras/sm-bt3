<?php 

$attributes = $attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes;

$attributes['class'] = isset($attributes['class']) ? $attributes['class'].' ' : '';
$attributes['class'] .= 'form-control';

$attributes['style'] = isset($attributes['style']) ? $attributes['style'].' ' : '';
$attributes['style'] .= 'width:80px; display:inline-block;';

echo $field->render($attributes);