<?php  

$attributes = $attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes;
$widget = $field->getWidget();
  
if ($widget->getOption('expanded'))
{
  $formatter = function($innerWidget, $inputs)
  {
    $inputClass = get_class($innerWidget) == 'sfWidgetFormSelectRadio' ? 'radio-inline' : 'checkbox-inline';

    $rows = array();
    foreach ($inputs as $input)
    {
      $rows[] = $innerWidget->renderContentTag('label', $input['input'].$innerWidget->getOption('label_separator').$input['label'], array('class' => $inputClass));
    }

    return !$rows ? '' : implode($innerWidget->getOption('separator'), $rows);
  };

  $widget->setOption('renderer_options', array('formatter' => $formatter));
}
else 
{
  $attributes['class'] = isset($attributes['class']) ? $attributes['class'].' ' : '';
  $attributes['class'] .= 'form-control';
}

echo $field->render($attributes);