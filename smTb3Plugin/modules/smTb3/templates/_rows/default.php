<?php $class = isset($class) ?  $class : 'form-group' ?>
<?php $help = isset($help) ? $help : null ?>
<?php $label = isset($label) ? $label : null ?>
<?php $attributes = isset($attributes) ? $attributes : array() ?>

<div class="<?php echo $class ?><?php $field->hasError() and print ' has-error' ?>">
  <?php if ($field->hasError() && $field->getError()->getMessageFormat()): ?>
    <div class="col-lg-offset-2 col-lg-10 text-danger"><b><span class="glyphicon glyphicon-hand-right"></span> <?php echo __($field->getError()->getMessageFormat(), $field->getError()->getArguments()) ?></b></div>
  <?php endif ?>
  <div>
    <?php echo $field->renderLabel($label, array('class' => 'col-lg-2 control-label')) ?>

    <div class="col-lg-10">        
      <?php try { ?>
        <?php include_partial('smTb3/fields/'.get_class($field->getWidget()), array('field' => $field, 'attributes' => $attributes)) ?>
      <?php } catch(sfRenderException $e) { ?>
        <?php echo $field->render($attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes) ?>
      <?php } ?>

      <?php if ($help): ?>
        <span class="help-block"><?php echo $help ?></span>
      <?php elseif ($help = $field->renderHelp()): ?>
        <span class="help-block"><?php echo $help ?></span>
      <?php endif ?>
    </div>
  </div>
</div>