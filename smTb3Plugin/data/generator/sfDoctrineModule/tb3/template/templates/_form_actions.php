<div class="form-group">
  <div class="col-lg-offset-2 col-lg-10">
    <?php foreach (array('new', 'edit') as $action): ?>
    <?php if ('new' == $action): ?>
    [?php if ($form->isNew()): ?]
    <?php else: ?>
    [?php else: ?]
    <?php endif; ?>
    <?php foreach ($this->configuration->getValue($action.'.actions') as $name => $params): ?>

    <?php if ('_delete' == $name): ?>
      <?php echo $this->addCredentialCondition('[?php echo $helper->linkToDelete($form->getObject(), '.$this->asPhp($params).') ?]', $params) ?>

    <?php elseif ('_list' == $name): ?>
      <?php echo $this->addCredentialCondition('[?php echo $helper->linkToList('.$this->asPhp($params).') ?]', $params) ?>

    <?php elseif ('_save' == $name): ?>
      <?php echo $this->addCredentialCondition('[?php echo $helper->linkToSave($form->getObject(), '.$this->asPhp($params).') ?]', $params) ?>

    <?php elseif ('_save_and_add' == $name): ?>
      <?php echo $this->addCredentialCondition('[?php echo $helper->linkToSaveAndAdd($form->getObject(), '.$this->asPhp($params).') ?]', $params) ?>

    <?php else: ?>
    [?php if (method_exists($helper, 'linkTo<?php echo $method = ucfirst(sfInflector::camelize($name)) ?>')): ?]
      <?php echo $this->addCredentialCondition('[?php echo $helper->linkTo'.$method.'($form->getObject(), '.$this->asPhp($params).') ?]', $params) ?>

    [?php else: ?]
      <?php $action = isset($params['action']) ? $params['action'] : 'List'.sfInflector::camelize($name); ?>
      <?php $url_params = '?'.$this->getPrimaryKeyUrlParams() ?>
      <?php $params['params']['class'] = isset($params['params']['class']) ? $params['params']['class'] : '' ?>
      <?php $params['params']['class'] .= 'btn '.(isset($params['btn_class']) ? $params['btn_class'] : 'btn-default') ?>
      <?php $icon = isset($params['icon_class']) ? '<span class="'.$params['icon_class'].'"></span> ' : '' ?>
      <?php $btn = '[?php echo link_to(\''.$icon.'\'.__(\''.$params['label'].'\', array(), \''.$this->getI18nCatalogue().'\'), \''.$this->getModuleName().'/'.$action.$url_params.', '.$this->asPhp($params['params']).') ?]' ?>
      <?php echo $this->addCredentialCondition($btn, $params)."\n" ?>    

    [?php endif; ?]
    <?php endif; ?>

    <?php endforeach; ?>
    <?php endforeach; ?>
    [?php endif; ?]
  </div>
</div>
