[?php if (!$pager->getNbResults()): ?]
  <div class="list-group">
    <div class="list-group-item text-muted">[?php echo __('No result', array(), 'sf_admin') ?]</div>
  </div>  
[?php else: ?]
  <table class="table table-striped table-hover sf_admin_list">
    <?php if ($this->configuration->getValue('list.batch_actions')): ?>
      <form action="[?php echo url_for('<?php echo $this->getUrlForAction('collection') ?>', array('action' => 'batch')) ?]" method="post">
    <?php endif; ?>    
    <thead>
      <tr>
<?php if ($this->configuration->getValue('list.batch_actions')): ?>
        <th><label class="checkbox-inline"><input id="sf_admin_list_batch_checkbox" type="checkbox" onclick="checkAll();" />&nbsp;</label></th>
<?php endif; ?>
        [?php include_partial('<?php echo $this->getModuleName() ?>/list_th_<?php echo $this->configuration->getValue('list.layout') ?>', array('sort' => $sort)) ?]
<?php if ($this->configuration->getValue('list.object_actions')): ?>
        <th class="text-right">&nbsp;</th>
<?php endif; ?>
      </tr>
    </thead>
    <?php if ($this->configuration->getValue('list.batch_actions')): ?>
    <tfoot>
      <tr>
        <th colspan="<?php echo count($this->configuration->getValue('list.display')) + ($this->configuration->getValue('list.object_actions') ? 1 : 0) + ($this->configuration->getValue('list.batch_actions') ? 1 : 0) ?>">
          [?php include_partial('<?php echo $this->getModuleName() ?>/list_batch_actions', array('helper' => $helper)) ?]
        </th>
      </tr>
    </tfoot>
    <?php endif; ?>
    <tbody>
      [?php foreach ($pager->getResults() as $i => $<?php echo $this->getSingularName() ?>): $odd = fmod(++$i, 2) ? 'odd' : 'even' ?]
        <tr>
<?php if ($this->configuration->getValue('list.batch_actions')): ?>
          [?php include_partial('<?php echo $this->getModuleName() ?>/list_td_batch_actions', array('<?php echo $this->getSingularName() ?>' => $<?php echo $this->getSingularName() ?>, 'helper' => $helper)) ?]
<?php endif; ?>
          [?php include_partial('<?php echo $this->getModuleName() ?>/list_td_<?php echo $this->configuration->getValue('list.layout') ?>', array('<?php echo $this->getSingularName() ?>' => $<?php echo $this->getSingularName() ?>)) ?]
<?php if ($this->configuration->getValue('list.object_actions')): ?>
          [?php include_partial('<?php echo $this->getModuleName() ?>/list_td_actions', array('<?php echo $this->getSingularName() ?>' => $<?php echo $this->getSingularName() ?>, 'helper' => $helper)) ?]
<?php endif; ?>
        </tr>
      [?php endforeach; ?]
    </tbody>
    <?php if ($this->configuration->getValue('list.batch_actions')): ?>
      </form>
    <?php endif; ?>    
  </table>
[?php endif; ?]

[?php if ($pager->getNbResults()): ?]
<div class="panel-footer clearfix">
  <div class="pull-right">
    [?php if ($pager->haveToPaginate()): ?]
      [?php include_partial('<?php echo $this->getModuleName() ?>/pagination', array('pager' => $pager)) ?]
    [?php endif; ?]
  </div>
  <div class="pull-right text-muted" style="margin: 4px 10px 0 0;">
    [?php echo format_number_choice('[0] no result|[1] 1 result|(1,+Inf] %1% results', array('%1%' => $pager->getNbResults()), $pager->getNbResults(), 'sf_admin') ?]
    [?php if ($pager->haveToPaginate()): ?]
      [?php echo __('(page %%page%%/%%nb_pages%%)', array('%%page%%' => $pager->getPage(), '%%nb_pages%%' => $pager->getLastPage()), 'sf_admin') ?]
    [?php endif; ?]
  </div>
</div>
[?php endif ?]

<script type="text/javascript">
/* <![CDATA[ */
function checkAll()
{
  var boxes = document.getElementsByTagName('input'); for(var index = 0; index < boxes.length; index++) { box = boxes[index]; if (box.type == 'checkbox' && box.className == 'sf_admin_batch_checkbox') box.checked = document.getElementById('sf_admin_list_batch_checkbox').checked } return true;
}
/* ]]> */
</script>
