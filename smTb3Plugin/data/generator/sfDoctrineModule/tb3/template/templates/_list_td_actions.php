<td class="text-right" style="white-space:nowrap;">
  <div class="sf_admin_list_object_actions" style="margin-top:-1px; margin-bottom:-2px;">
<?php foreach ($this->configuration->getValue('list.object_actions') as $name => $params): ?>
  <?php if ('_delete' == $name): ?>
      <?php echo $this->addCredentialCondition('[?php echo $helper->linkToListDelete($'.$this->getSingularName().', '.$this->asPhp($params).') ?]', $params) ?>

  <?php elseif ('_edit' == $name): ?>
      <?php echo $this->addCredentialCondition('[?php echo $helper->linkToListEdit($'.$this->getSingularName().', '.$this->asPhp($params).') ?]', $params) ?>

  <?php else: ?>
      <?php $action = isset($params['action']) ? $params['action'] : 'List'.sfInflector::camelize($name); ?>
      <?php $url_params = '?'.$this->getPrimaryKeyUrlParams() ?>
      <?php $params['params']['class'] = isset($params['params']['class']) ? $params['params']['class'] : '' ?>
      <?php $params['params']['class'] .= 'btn btn-xs '.(isset($params['btn_class']) ? $params['btn_class'] : 'btn-default') ?>
      <?php $icon = isset($params['icon_class']) ? '<span class="'.$params['icon_class'].'"></span> ' : '' ?>
      <?php $btn = '[?php echo link_to(\''.$icon.'\'.__(\''.$params['label'].'\', array(), \''.$this->getI18nCatalogue().'\'), \''.$this->getModuleName().'/'.$action.$url_params.', '.$this->asPhp($params['params']).') ?]' ?>
      <?php echo $this->addCredentialCondition($btn, $params)."\n" ?>

  <?php endif; ?>
<?php endforeach; ?>
  </div>
</td>
<style>
  .sf_admin_list_object_actions {
    visibility: hidden;
  }
  .sf_admin_list tbody tr:hover .sf_admin_list_object_actions {
    visibility: visible;
  }
</style>