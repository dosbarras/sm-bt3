<div class="input-group">
  <select name="batch_action" class="form-control">
    <option value="">[?php echo __('Choose an action', array(), 'sf_admin') ?]</option>
  <?php foreach ((array) $this->configuration->getValue('list.batch_actions') as $action => $params): ?>
    <?php echo $this->addCredentialCondition('<option value="'.$action.'">[?php echo __(\''.$params['label'].'\', array(), \'sf_admin\') ?]</option>', $params) ?>
  <?php endforeach; ?>
  </select>
  [?php $form = new BaseForm(); if ($form->isCSRFProtected()): ?]
    <input type="hidden" name="[?php echo $form->getCSRFFieldName() ?]" value="[?php echo $form->getCSRFToken() ?]" />
  [?php endif; ?]
  <span class="input-group-btn">
    <button type="submit" class="btn btn-default" title="[?php echo __('Apply', array(), 'sf_admin') ?]"><span class="glyphicon glyphicon-ok"></span></button>
  </span>
</div>