[?php use_stylesheets_for_form($form) ?]
[?php use_javascripts_for_form($form) ?]

<div class="sf_admin_filter">
  [?php if ($form->hasGlobalErrors()): ?]
    [?php echo $form->renderGlobalErrors() ?]
  [?php endif; ?]

  <form class="form-horizontal" action="[?php echo url_for('<?php echo $this->getUrlForAction('collection') ?>', array('action' => 'filter')) ?]" method="post">

    [?php foreach ($configuration->getFormFilterFields($form) as $name => $field): ?]
    [?php if ((isset($form[$name]) && $form[$name]->isHidden()) || (!isset($form[$name]) && $field->isReal())) continue ?]
      [?php include_partial('<?php echo $this->getModuleName() ?>/filters_field', array(
        'name'       => $name,
        'attributes' => $field->getConfig('attributes', array()),
        'label'      => $field->getConfig('label'),
        'help'       => $field->getConfig('help'),
        'form'       => $form,
        'field'      => $field,
        'class'      => 'form-group sf_admin_'.strtolower($field->getType()).' sf_admin_filter_field_'.$name,
      )) ?]
    [?php endforeach; ?]    
    
    [?php echo $form->renderHiddenFields() ?]
    
    <div class="form-group" style="margin-bottom:0">
      <div class="col-lg-offset-2 col-lg-10">
        <input class="btn btn-default" type="submit" value="[?php echo __('Filter', array(), 'sf_admin') ?]" />  
        [?php if ($isFiltering): ?]
          [?php echo link_to(__('Reset', array(), 'sf_admin'), '<?php echo $this->getUrlForAction('collection') ?>', array('action' => 'filter'), array('query_string' => '_reset', 'method' => 'post', 'class' => 'btn btn-default')) ?]
        [?php endif ?]
      </div>
    </div>

  </form>
</div>
