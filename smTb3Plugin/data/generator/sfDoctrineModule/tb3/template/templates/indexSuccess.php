[?php use_helper('I18N', 'Date') ?]
[?php include_partial('<?php echo $this->getModuleName() ?>/assets') ?]

<div class="page-header">
  <h1>[?php echo <?php echo $this->getI18NString('list.title') ?> ?]</h1>
</div>

[?php include_partial('<?php echo $this->getModuleName() ?>/flashes') ?]

[?php include_partial('<?php echo $this->getModuleName() ?>/list_header', array('pager' => $pager)) ?]

<div class="panel panel-default">
  
  <div class="panel-heading">
    <?php if ($this->configuration->hasFilterForm()): ?>
      [?php $isFiltering = (bool) count($sf_user->getAttribute('<?php echo $this->getModuleName() ?>.filters', array(), 'admin_module')) ?]
      <a href="#filters" class="btn [?php if($isFiltering): ?]btn-warning[?php else: ?]btn-default[?php endif ?]" data-toggle="collapse"><span class="glyphicon glyphicon-search"></span> [?php echo __('Search', array(), 'sf_admin') ?]</a>
    <?php endif ?>
    [?php include_partial('<?php echo $this->getModuleName() ?>/list_actions', array('helper' => $helper)) ?]
  </div>
  
  <?php if ($this->configuration->hasFilterForm()): ?>
    <div class="collapse[?php if($isFiltering): ?] in[?php endif ?]" id="filters">
      <div class="panel-body">
        [?php include_partial('<?php echo $this->getModuleName() ?>/filters', array('form' => $filters, 'configuration' => $configuration, 'isFiltering' => $isFiltering)) ?]
      </div>
    </div>
  <?php endif; ?>
  
  [?php include_partial('<?php echo $this->getModuleName() ?>/list', array('pager' => $pager, 'sort' => $sort, 'helper' => $helper)) ?]
</div>

[?php include_partial('<?php echo $this->getModuleName() ?>/list_footer', array('pager' => $pager)) ?]
