[?php if ($field->isPartial()): ?]
  [?php include_partial('<?php echo $this->getModuleName() ?>/'.$name, array('type' => 'filter', 'form' => $form, 'attributes' => $attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes)) ?]
[?php elseif ($field->isComponent()): ?]
  [?php include_component('<?php echo $this->getModuleName() ?>', $name, array('type' => 'filter', 'form' => $form, 'attributes' => $attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes)) ?]
[?php else: ?]
  <div class="[?php echo $class ?][?php $form[$name]->hasError() and print ' has-error' ?]">
    [?php if ($form[$name]->hasError()): ?]
      <div class="col-lg-offset-2 col-lg-10 text-danger"><b><span class="glyphicon glyphicon-hand-right"></span> [?php echo $form[$name]->getError() ?]</b></div>
    [?php endif ?]
    <div>
      [?php echo $form[$name]->renderLabel($label, array('class' => 'col-lg-2 control-label')) ?]

      <div class="col-lg-10">
        [?php try { ?]
          [?php include_partial('smTb3/fields/'.get_class($form[$name]->getWidget()), array('field' => $form[$name], 'attributes' => $attributes)) ?]
        [?php } catch(sfRenderException $e) { ?] 
          [?php echo $form[$name]->render($attributes instanceof sfOutputEscaper ? $attributes->getRawValue() : $attributes) ?]
        [?php } ?]
        
        [?php if ($help): ?]
          <span class="help-block">[?php echo __($help, array(), '<?php echo $this->getI18nCatalogue() ?>') ?]</span>
        [?php elseif ($help = $form[$name]->renderHelp()): ?]
          <span class="help-block">[?php echo $help ?]</span>
        [?php endif; ?]
      </div>
    </div>
  </div>
[?php endif; ?]
