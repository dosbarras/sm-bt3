[?php if ($value): ?]
  <span class="glyphicon glyphicon-ok" title="[?php echo __('Checked', array(), 'sf_admin') ?]"></span>
[?php else: ?]
  &nbsp;
[?php endif; ?]
