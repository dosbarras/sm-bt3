<?php if ($actions = $this->configuration->getValue('list.actions')): ?>
  <?php foreach ($actions as $name => $params): ?>

    <?php if ('_new' == $name): ?>
      <?php echo $this->addCredentialCondition('[?php echo $helper->linkToNew('.$this->asPhp($params).') ?]', $params)."\n" ?>

    <?php else: ?>
      <?php $action = isset($params['action']) ? $params['action'] : 'List'.sfInflector::camelize($name); ?>
      <?php $url_params = '\'' ?>
      <?php $params['params']['class'] = isset($params['params']['class']) ? $params['params']['class'] : '' ?>
      <?php $params['params']['class'] .= 'btn '.(isset($params['btn_class']) ? $params['btn_class'] : 'btn-default') ?>
      <?php $icon = isset($params['icon_class']) ? '<span class="'.$params['icon_class'].'"></span> ' : '' ?>
      <?php $btn = '[?php echo link_to(\''.$icon.'\'.__(\''.$params['label'].'\', array(), \''.$this->getI18nCatalogue().'\'), \''.$this->getModuleName().'/'.$action.$url_params.', '.$this->asPhp($params['params']).') ?]' ?>
      <?php echo $this->addCredentialCondition($btn, $params)."\n" ?>

    <?php endif; ?>
  <?php endforeach; ?>
<?php endif; ?>
