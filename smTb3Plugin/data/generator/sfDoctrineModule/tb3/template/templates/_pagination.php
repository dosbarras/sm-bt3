<ul class="pagination pagination-sm" style="margin:0;">
  
  <li>
    <a href="[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=1" title="[?php echo __('First page', array(), 'sf_admin') ?]">
      &laquo;
    </a>
  </li>

  <li>
    <a href="[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=[?php echo $pager->getPreviousPage() ?]" title="[?php echo __('Previous page', array(), 'sf_admin') ?]">
      &lsaquo;
    </a>
  </li>
  
  [?php foreach ($pager->getLinks() as $page): ?]
    [?php if ($page == $pager->getPage()): ?]
      <li class="active">
        <span>[?php echo $page ?]</span>
      </li>
    [?php else: ?]
      <li>
        <a href="[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=[?php echo $page ?]">[?php echo $page ?]</a>
      </li>
    [?php endif; ?]
  [?php endforeach; ?]

  <li>
    <a href="[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=[?php echo $pager->getNextPage() ?]" title="[?php echo __('Next page', array(), 'sf_admin') ?]">
      &rsaquo;
    </a>
  </li>

  <li>
    <a href="[?php echo url_for('@<?php echo $this->getUrlForAction('list') ?>') ?]?page=[?php echo $pager->getLastPage() ?]" title="[?php echo __('Last page', array(), 'sf_admin') ?]">
      &raquo;
    </a>
  </li>
  
</ul>
