smTb3Plugin
===========

Plugin que proporciona un theme de twitter bootstrap 3 en el admin generator.

Instalación
-----------

Vía composer:

    composer require dosbarras/sm-tb3:dev-master

Habilitar el plugin en ProjectConfiguration, pero antes establecer la ubicación del mismo:

    $this->setPluginPath('smTb3Plugin', dirname(__FILE__).'/../vendor/dosbarras/sm-tb3/smTb3Plugin');

    $this->enablePlugins(..., 'smTb3Plugin');

Limpiar la cache:

    rm -rf cache/*

Si tienes APC instalado en Apache, recargarlo:

    sudo service apache2 reload

El plugin no incorpora ningún archivo de bootstrap, por lo que es responsabilidad del
programador incluírlo en el layout en las plantillas que considere necesario.

### Layout básico de ejemplo para comenzar a desarrollar usando CDNs

    <!DOCTYPE html>
    <html lang="es">
      <head>
        <?php include_http_metas() ?>
        <?php include_metas() ?>
        <?php include_title() ?>

        <?php use_stylesheet('//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css') ?>
        <?php use_stylesheet('//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-theme.min.css') ?>

        <?php include_stylesheets() ?>
      </head>
      <body>

        <!-- aquí puedes crear tu menú de navegación bootstrap -->

        <div class="container">
          <?php echo $sf_content ?>
        </div>

        <?php use_javascript('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js') ?>
        <?php use_javascript('//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.min.js') ?>

        <?php include_javascripts() ?>
      </body>
    </html>

Cuando tengas que crear plantillas manualmente, te puedes basar en las plantillas
autogeneradas que están en cache para tener un diseño parejo.

Uso
---

### Al generar un nuevo módulo

Para utilizar el theme proporcionado al generar un nuevo módulo sólo agregar
la opción `--theme=tb3`, ej:

    php symfony doctrine:generate-admin [app] [model] --theme=tb3

### En módulos ya generados

Si ya tienes los módulos generados puedes establecer la opción `theme: tb3` 
en el generator.yml y luego limpiar cache.

Redefinir estilos
-----------------

Es posible redefinir los estilos para cada tipo de widget ya que el plugin 
utiliza plantillas para este fin (ver el módulo incluído).

Creando un módulo en la aplicación con el mismo nombre que el del plugin y 
agregando o modificando una plantilla para un widget puedes cambiar la 
forma en que se renderiza.